<?php
// Template Name: Lista Lobinhos
?>
<?php get_header();?>
    <main id="main-lb">
        <div id="topbody">
            <div id="searchadd">
                <div id="search">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/assets/lupinha.png" id="searchimg">                   
                    <input type="text" id="searchtext">
                </div>
                <a id="btnaddlobo" href="../adicionar-lobinho/adicionar-lobinho.html">+ Lobo</a>
            </div>
            <div id="checkadotados">
                <input type="checkbox" id="checkbox">
                <p id="checktext">Ver lobinhos adotados</p>
            </div>
        </div> 
        <div id="menulobinhos">
            <div id="exemploLobos">
                <div id="content-ex">
                    <div class="escrito-lobosEx">
                        <h2>Lobos Exemplos</h2>
                    </div>
                    <div id="lobos-content">
                    <?php 
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $the_query = new WP_Query( array(
                            'posts_per_page' => 4,
                            'paged' => $paged
                        ) );
                    ?>
                    <?php $numero = 0 ?>
                    <?php 
                        while ($the_query -> have_posts()) : $the_query -> the_post();
                    ?>

                    <?php if($numero % 2 == 0) { ?>
                    
                        <div class="bloco-lobo left">
                            <div class="img-lobo">
                                <?php if( get_field('lobo_foto') ): ?>
                                    <img class="img-left" src="<?php the_field('lobo_foto'); ?>">
                                <?php endif; ?>
                            </div>
                            <div class="info-lobo left-info">
                                <h1><b><?php the_field('lobo_titulo'); ?></b></h1>
                                <h4>Idade: <?php the_field('lobo_idade'); ?> anos</h4>
                                <p><b><?php the_field('lobo_descricao'); ?></b></p>
                            </div>
                        </div>
                    <?php } else { ?>
                    <div class="bloco-lobo right">
                        <div class="info-lobo-right right-info">
                            <h1><b><?php the_field('lobo_titulo'); ?></b></h1>
                            <h4>Idade: <?php the_field('lobo_idade'); ?> anos</h4>
                            <p><b><?php the_field('lobo_descricao'); ?></b></p>
                        </div>
                        <div class="img-lobo">
                            <?php if( get_field('lobo_foto') ): ?>
                                <img class="img-left" src="<?php the_field('lobo_foto'); ?>">
                            <?php endif; ?>  
                        </div>
                    </div>
                    <?php }
                        $numero++; ?>
                    <?php endwhile; ?>
                        <div class="pagination">
                            <?php 
                                echo paginate_links( array(
                                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                    'total'        => $query->max_num_pages,
                                    'current'      => max( 1, get_query_var( 'paged' ) ),
                                    'format'       => '?paged=%#%',
                                    'show_all'     => false,
                                    'type'         => 'plain',
                                    'end_size'     => 2,
                                    'mid_size'     => 1,
                                    'prev_next'    => true,
                                    'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                                    'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                                    'add_args'     => false,
                                    'add_fragment' => '',
                                ) );
                            ?>
                        </div>
                        <?php wp_reset_postdata();  ?>

                    </div>
                   
                </div>
            </div>
        </div>    
    </main>
<?php get_footer();?>