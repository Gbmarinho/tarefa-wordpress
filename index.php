<?php get_header();?>
    <main>

        <div id="adoteLobinho">
            <div>
                <h1 class="fonte">Adote um Lobinho</h1>
                <span class="barra"> </span>
                <p>
                    É claro que o consenso sobre a necessidade de<br>
                    qualificação apresenta tendências no sentido de aprovar a<br>
                    manutenção das regras de condutas normativas 
                </p>
            </div>
        </div>

        <div id="sobre">

            <div>
                <h2>Sobre</h2>
                <p>
                    Não obstante, o surgimento do cómercio virtual faz parte de um processo de gerenciamento do levantamento
                    das variávei envolvidas. Não obstante, o surgimento do cómercio virtual faz parte de um processo de
                    gerenciamento do levantamento das variávei envolvidas.Não obstante, o surgimento do cómercio virtual faz
                    parte de um processo de gerenciamento do levantamento das variávei envolvidas.Não obstante, o surgimento
                    do cómercio virtual faz parte de um processo de gerenciamento do levantamento das variávei envolvidas.
                </p>
            </div>

        </div>

        <div id="valores">
            <div id="escrito-valores">
                <h2>
                    Valores
                </h2>
            </div>
            <div id="content-valores">
                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-1">                         
                    </div> 
                    <div class="text-block">
                        <h3>Proteção</h3>
                        <p>Assim mesmo, o desenvolvimento continuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-2">
                    </div>
                    <div class="text-block">
                        <h3>Carinho</h3>
                        <p>Assim mesmo, o desenvolvimento continuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-3">
                    </div>
                    <div class="text-block">
                        <h3>Companheirismo</h3>
                        <p>Assim mesmo, o desenvolvimento continuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-4">
                    </div>
                    <div class="text-block">
                        <h3>Resgate</h3>
                        <p>Assim mesmo, o desenvolvimento continuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </div>

            </div>
        </div>

        <div id="exemploLobos">
            <div id="content-ex">
                <div class="escrito-lobosEx">
                    <h2>Lobos Exemplos</h2>
                </div>
                <div id="lobos-content">
                    <div class="bloco-lobo left">
                        <div class="img-lobo">
                            <img class="img-left" src="https://www.netvasco.com.br/news/noticias16/arquivos/20210710-131925-1-.jpg">
                        </div>
                        <div class="info-lobo left-info">
                            <h1><b>Vasco</b></h1>
                            <h4>Idade: 124 anos</h4>
                            <p><b>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</b></p>
                        </div>
                    </div>
                    <div class="bloco-lobo right">
                        <div class="info-lobo-right right-info">
                            <h1><b>Cerberus</b></h1>
                            <h4>Idade: 100000 anos</h4>
                            <p><b>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</b></p>
                        </div>
                        <div class="img-lobo">
                            <img class="img-right" src="https://seresmitologicos.com.br/wp-content/uploads/cerberus.jpg">  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php get_footer();?>