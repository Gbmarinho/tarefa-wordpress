
<div class="bloco-lobo left">
                            <div class="img-lobo">
                                <?php if( get_field('lobo_foto') ): ?>
                                    <img class="img-left" src="<?php the_field('lobo_foto'); ?>">
                                <?php endif; ?>
                            </div>
                            <div class="info-lobo left-info">
                                <h1><b><?php the_field('lobo_titulo'); ?></b></h1>
                                <h4>Idade: <?php the_field('lobo_idade'); ?> anos</h4>
                                <p><b><?php the_field('lobo_descricao'); ?></b></p>
                            </div>
                        </div>











<div class="bloco-lobo right">
                        <div class="info-lobo-right right-info">
                            <h1><b><?php the_field('lobo_titulo'); ?></b></h1>
                            <h4>Idade: <?php the_field('lobo_idade'); ?> anos</h4>
                            <p><b><?php the_field('lobo_descricao'); ?></b></p>
                        </div>
                        <div class="img-lobo">
                            <?php if( get_field('lobo_foto') ): ?>
                                <img class="img-left" src="<?php the_field('lobo_foto'); ?>">
                            <?php endif; ?>  
                        </div>
                    </div>