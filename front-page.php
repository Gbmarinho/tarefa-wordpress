<?php
// Template Name: home
?>
<?php get_header();?>
    <main>
        <div id="adoteLobinho">
            <div>
                <h1 class="fonte"><?php the_field('titulo_inicial'); ?></h1>
                <span class="barra"> </span>
                <p>
                    <?php the_field('descricao_inicial'); ?>
                </p>
            </div>
        </div>

        <div id="sobre">

            <div>
                <h2><?php the_field('segundo_titulo'); ?></h2>
                <p>
                    <?php the_field('descricao_segundo'); ?>
                </p>
            </div>

        </div>

        <div id="valores">
            <div id="escrito-valores">
                <h2>
                    <?php the_field('terceiro_titulo'); ?>
                </h2>
            </div>
            <div id="content-valores">
                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-1">                         
                    </div> 
                    <div class="text-block">
                        <h3><?php the_field('blocos_titulo_bloco_1'); ?></h3>
                        <p><?php the_field('blocos_descricao_bloco_1'); ?></p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-2">
                    </div>
                    <div class="text-block">
                        <h3><?php the_field('blocos_titulo_bloco_2'); ?></h3>
                        <p><?php the_field('blocos_descricao_bloco_2'); ?></p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-3">
                    </div>
                    <div class="text-block">
                        <h3><?php the_field('blocos_titulo_bloco_3'); ?></h3>
                        <p><?php the_field('blocos_descricao_bloco_3'); ?></p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-4">
                    </div>
                    <div class="text-block">
                        <h3><?php the_field('blocos_titulo_bloco_4'); ?></h3>
                        <p><?php the_field('blocos_descricao_bloco_4'); ?></p>
                    </div>
                </div>

            </div>
        </div>

        <div id="exemploLobos">
            <div id="content-ex">
                <div class="escrito-lobosEx">
                    <h2><?php the_field('quarto_titulo'); ?></h2>
                </div>
        
                <div id="lobos-content">
                    <?php 
                        $the_query = new WP_Query('posts_per_page=2');
                    ?>
                    <?php $numero = 0 ?>
                    <?php 
                        while ($the_query -> have_posts()) : $the_query -> the_post();
                    ?>

                    <?php if($numero % 2 == 0) { ?>
                    <div class="bloco-lobo left">
                        <div class="img-lobo">
                            <?php if( get_field('lobo_foto') ): ?>
                                <img class="img-left" src="<?php the_field('lobo_foto'); ?>">
                            <?php endif; ?>
                        </div>
                        <div class="info-lobo left-info">
                            <h1><b><?php the_field('lobo_titulo'); ?></b></h1>
                            <h4>Idade: <?php the_field('lobo_idade'); ?> anos</h4>
                            <p><b><?php the_field('lobo_descricao'); ?></b></p>
                        </div>
                    </div>
                    <?php }
                    else { ?>
                    <div class="bloco-lobo right">
                        <div class="info-lobo-right right-info">
                            <h1><b><?php the_field('lobo_titulo'); ?></b></h1>
                            <h4>Idade: <?php the_field('lobo_idade'); ?> anos</h4>
                            <p><b><?php the_field('lobo_descricao'); ?></b></p>
                        </div>
                        <div class="img-lobo">
                            <?php if( get_field('lobo_foto') ): ?>
                                <img class="img-left" src="<?php the_field('lobo_foto'); ?>">
                            <?php endif; ?>  
                        </div>
                    </div>
                    <?php }
                        $numero++; ?>
                    <?php
                        endwhile;
                        wp_reset_postdata(); 
                    ?>
                </div>
            </div>
        </div>
    </main>

<?php get_footer();?>