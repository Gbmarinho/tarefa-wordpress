<?php
// Template Name: somos
?>
<?php get_header();?>

    <main id="content-qs">
        <?php the_title('<h1 id="title-qs">', '</h1>');?>
        <div id="abt-qs"><?php the_content();?></div>
    </main>


<?php get_footer();?>