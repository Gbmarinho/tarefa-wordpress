
<footer>
        <div id="content-footer">
            <div id="contato">
                <div id="mapa">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.760673396595!2d-43.1333917!3d-22.9063556!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb39c7c0639fbc9e8!2sIN%20Junior%20-%20Empresa%20Junior%20de%20Computa%C3%A7%C3%A3o%20da%20UFF!5e0!3m2!1spt-BR!2sbr!4v1659217850287!5m2!1spt-BR!2sbr" width="250" height="175" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div id="tudo-contato">
                    <div class="div-partes"><img class="img-footer map" src="<?php echo get_stylesheet_directory_uri()?>/assets/Vector.png"><p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa<br>Viagem, Niterói - RJ, 24210-315</p></div>
                    <div class="div-partes"><img class="img-footer" src="<?php echo get_stylesheet_directory_uri()?>/assets/telefone.png"><p>(99) 99999-9999</p></div>
                    <div class="div-partes"><img class="img-footer" src="<?php echo get_stylesheet_directory_uri()?>/assets/email.png"><p>salve-lobos@lobINhos.com</p></div>
                    <div class="centraliza"><a class="botao-quemSomos" href="http://projetolobo.local/quem-somos/"><p>Quem Somos</p></a></div>
                </div>
            </div>
            <div id="Desenvolvido">
                <div class="escrito-desenvolvido">
                    <p>Desenvolvido com</p>
                </div>
                <div class="img-desenvolvido">
                    <img class="img-paws" src="<?php echo get_stylesheet_directory_uri()?>/assets/paws.png">
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>